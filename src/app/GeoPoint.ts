export interface GeoPoint {
    name: string;
    latitude: number;
    longitude: number;
}
