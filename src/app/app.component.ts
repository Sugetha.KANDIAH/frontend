import {Component, EventEmitter, Output} from '@angular/core';
import {GeoPoint} from './geo-point-table/geo-point-table.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TravelingSalesman';
  geoPoints = [];
  geoPointRow = [];
  onReceivedEvent($event) {
    this.geoPoints.push($event[0]);
    console.log(this.geoPoints[0]);
  }
  onReceivedEventRow($event) {
    this.geoPointRow.push($event[0]);
    console.log(this.geoPointRow[0]);
  }
}
