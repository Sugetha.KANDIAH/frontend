import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { GeoPointTableComponent } from './geo-point-table/geo-point-table.component';
import { GeoPointRowComponent } from './geo-point-row/geo-point-row.component';

@NgModule({
  declarations: [
    AppComponent,
    GeoPointTableComponent,
    GeoPointRowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
