import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
// import {GeoPoint} from '../GeoPoint';

export class GeoPoint {
   name: string;
   latitude = 0.0;
   longitude = 0.0;
   constructor(name, lattitude, longitude) {
     this.name = name;
     this.latitude = lattitude;
     this.longitude = longitude;
   }
}

@Component({
  selector: 'app-geo-point-table',
  templateUrl: './geo-point-table.component.html',
  styleUrls: ['./geo-point-table.component.css']
})

export class GeoPointTableComponent implements OnInit, GeoPoint {
  constructor() { }
  points = [];
  name: string;
  latitude: number;
  longitude: number;
  @Output() tableChange = new EventEmitter < GeoPoint[] >() ;
  ngOnInit() {
  }
  addPoint() {
        const tabPoint = [];
        tabPoint.push(new GeoPoint(this.name, this.latitude, this.longitude));
        this.points.push(tabPoint[0]);
        this.tableChange.emit(tabPoint);
  }


}

