import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GeoPoint} from '../geo-point-table/geo-point-table.component';
// import {GeoPoint} from '../GeoPoint';

@Component({
  selector: 'app-geo-point-row',
  templateUrl: './geo-point-row.component.html',
  styleUrls: ['./geo-point-row.component.css']
})
export class GeoPointRowComponent implements OnInit {
  name: string;
  latitude: number;
  longitude: number;

  constructor(        ) { }
  @Input()point: GeoPoint;
  @Output() pointChange = new EventEmitter<GeoPoint[]>();
  pointsRow = [];
  ngOnInit() {
  }


  addPointRow(point: GeoPoint) {
    const tabPoint = [];
    tabPoint.push(new GeoPoint(point.name, point.latitude, point.longitude));
    this.pointsRow.push(tabPoint[0]);
    this.pointChange.emit(tabPoint);
    console.log('ADD POINT ROW', this.pointChange);
  }

}
